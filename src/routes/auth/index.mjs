import Router from 'express';

import slack from './slack';

const router = Router({ mergeParams: true });

router.use('/slack', slack);

export default router;
