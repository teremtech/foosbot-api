import Router from 'express';
import passport from 'passport';

const router = Router({ mergeParams: true });

router.get('/callback',
  passport.authenticate('slack', { session: false }),
  (req, res) => {
    res.send('<p>Greet and React was successfully installed on your team.</p>');
  },
  (err, req, res) => {
    res.status(500).send(`<p>Greet and React failed to install</p> <pre>${err}</pre>`);
  },
);

router.get('/', passport.authenticate('slack', { scope: ['bot'] }));

export default router;
