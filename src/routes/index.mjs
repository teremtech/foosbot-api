import Router from 'express';

import routes from './v1';
import auth from './auth';

const router = Router({ mergeParams: true });

router.use('/auth/', auth);
router.use('/api/v1/', routes);

export default router;
