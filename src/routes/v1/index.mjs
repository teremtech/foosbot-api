import Router from 'express';

import tables from './tables';

const router = Router({ mergeParams: true });

router.use('/tables', tables);

export default router;
