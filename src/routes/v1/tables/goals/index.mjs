import Router from 'express';
import sequelize from 'sequelize';

import postMessage from '../../../../utils/postMessage';

import {
  POST_GOAL_SCHEMA,
} from './schema';

const { Op } = sequelize;
const router = Router({ mergeParams: true });

router.post('/:tableId/goals', POST_GOAL_SCHEMA, async (req, res) => {
  const { tableId } = req.params;
  const { team } = req.body;
  const { game: Game, gameGoal: Goal } = req.app.get('db');

  // Find the current game and terminate if one is not found
  const activeGame = await Game.findOne({ where: { tableId, finishDate: { [Op.eq]: null } } });
  if (!activeGame) {
    return res.boom.notFound('Could not find an active game running for this table.');
  }

  // Pull down needed variables from active game
  const {
    channelId, gameId, maximumGoals, teamId,
  } = activeGame;

  // Get Slack WebClient for this team
  const slack = await req.getSlackClientByTeamId(teamId);
  if (!slack) {
    return res.boom.internal('Could not get Slack client for this specific team!');
  }

  // Don't allow a goal to get scored if the game hasn't started yet
  if (!activeGame.startDate) {
    postMessage(slack, channelId, 'Slow down, you haven\'t even started the game yet!');
    return res.boom.notFound('Game has not been started yet');
  }

  await Goal.create({ gameId, team, playerId: req.body.playerId });

  const teamGoalCount = await Goal.count({ where: { gameId, team } });

  if (teamGoalCount >= maximumGoals) {
    postMessage(slack, channelId, `Team ${team} has won the game!`);
    activeGame.finishDate = sequelize.fn('NOW');
    activeGame.save();
  } else {
    postMessage(slack, channelId,
      `Boom!  The ${team} team scored a goal.\r\n`
      + `Team ${team} now has ${teamGoalCount} goals`);
  }

  return res.send();
});

export default router;
