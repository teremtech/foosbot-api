import Celebrate from 'celebrate';

import {
  TEAM_A,
  TEAM_B,
} from '../../../../constants';

const { celebrate, Joi } = Celebrate;

// eslint-disable-next-line import/prefer-default-export
export const POST_GOAL_SCHEMA = celebrate({
  body: {
    team: Joi.string().valid(TEAM_A, TEAM_B).required(),
  },
});
