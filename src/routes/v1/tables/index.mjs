import Router from 'express';

import goals from './goals';

const router = Router({ mergeParams: true });

router.use(goals);

export default router;
