export const TEAM_A = 'A';
export const TEAM_B = 'B';

export const DEFAULT_MAX_GOALS = 9;
export const DEFAULT_MAX_PARTICIPANTS = 2;
