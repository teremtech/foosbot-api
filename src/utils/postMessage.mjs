export default function postMessage(slack, channel, text) {
  slack.chat.postMessage({ channel, text }, () => {});
}
