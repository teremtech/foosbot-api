import {
  TEAM_A,
  TEAM_B,
} from '../constants';


export default function (team) {
  switch (team.toUpperCase().trim()) {
    case TEAM_A: return TEAM_B;
    case TEAM_B: return TEAM_A;
    default: throw new Error(`Unexpected team "${team}"`);
  }
}
