export default async promise => promise
  .then(data => [undefined, data])
  .catch(err => [err, undefined]);
