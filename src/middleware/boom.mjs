// This middleware is strongly influenced by libraries like express-boom
// Express' error-handling sucks, so we're using Hapi's boom helper
// It allows us to hit certain errors in a nice and uniform way while also providing right headers
import boom from 'boom';

// Get all boom functions (i.e. notFound, forbidden, unauthorized, etc)
const keys = Object.getOwnPropertyNames(boom).filter(key => typeof boom[key] === 'function');

// Helper function to set proper headers and send response with correct status/body
function respond(res, boomed, data) {
  // If we've received the optional data object, attach it to the sent payload
  if (data && typeof data === 'object' && !Array.isArray(data)) {
    Object.assign(boomed.payload, data);
  }

  for (const header in boomed.headers) {
    if (!Object.prototype.hasOwnProperty.call(boomed, header)) continue;
    res.setHeader(header, boomed.headers[header]);
  }

  return res.status(boomed.statusCode).json(boomed.payload);
}

export default function middleware(req, res, next) {
  // If the response object already has a boom property, something has gone awry
  if (res.boom) {
    throw new Error('boom already exists on response object');
  }

  // Create boom object
  res.boom = Object.create({});

  // Create wrapper methods for each boom function and supply to our response's boom object
  for (const key of keys) {
    res.boom[key] = (message, data) => {
      const boomed = boom[key](message, data).output;
      return respond(res, boomed, data);
    };
  }

  next();
}
