import * as messages from './messages';

export default [
  { id: messages.name, handler: messages.handler },
];
