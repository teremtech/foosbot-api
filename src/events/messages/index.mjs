import createGame from './createGame';
import startGame from './startGame';

/* eslint-disable indent */
export const name = 'app_mention';

export async function handler(event, body) {
  const slack = await this.getSlackClientByTeamId(body.team_id);
  if (!slack) {
    return;
  }

  const postMessage = text => slack.chat.postMessage({ channel: event.channel, text }, () => { });

  let { text } = event;

  // Remove the actual mention
  text = text.replace(/^\s*<@[a-zA-Z0-9]+>\s*/, '').trim();
  const command = text.split(/\s+/)[0].toLowerCase();

  switch (command) {
    case 'create':
      createGame(this, slack, event, body);
      return;

    case 'start':
      startGame(this, slack, event, body);
      break;

    case 'add':
      postMessage('you tryin to add a player, huh, coward?');
      break;

    default:
      postMessage('i do not understand your nonsense');
  }
}
