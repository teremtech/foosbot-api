/* eslint-disable no-await-in-loop */
import sequelize from 'sequelize';

import {
  TEAM_A,
  TEAM_B,
} from '../../constants/index';

import postMessage from '../../utils/postMessage';

const { Op } = sequelize;

export default async function (instance, slack, event, body) {
  const { game: Game, gamePlayer: GamePlayer } = instance.db;
  const { channel: channelId } = event;

  const _postMessage = message => postMessage(slack, channelId, message);

  let waitingGame = await Game.findOne({
    where: {
      channelId,
      finishDate: { [Op.eq]: null },
    },
  });

  if (!waitingGame) {
    _postMessage('No game is currently waiting to start?');
    return;
  }

  if (waitingGame && waitingGame.startDate) {
    return;
  }

  const { gameId } = waitingGame;
  const teamAPlayers = await GamePlayer.count({ where: { gameId, team: TEAM_A } });
  const teamBPlayers = await GamePlayer.count({ where: { gameId, team: TEAM_B } });

  if (teamAPlayers === 0 || teamBPlayers === 0) {
    _postMessage('Cannot start a game with no players on a team');
    return;
  }

  if (teamAPlayers !== teamBPlayers) {
    _postMessage('Both teams must have the same amount of players!');
    return;
  }


  waitingGame.startDate = new Date();
  await waitingGame.save();

  _postMessage('Here we go...');
}
