/* eslint-disable no-await-in-loop */
import sequelize from 'sequelize';

import postMessage from '../../utils/postMessage';

const { Op } = sequelize;

export default async function (instance, slack, event, body) {
  const { game: Game, teamTable: TeamTable } = instance.db;
  const { channel: channelId } = event;
  const { team_id: teamId } = body;

  const outcome = message => postMessage(slack, channelId, message);

  let activeGame = await Game.findOne({
    where: {
      channelId,
      finishDate: { [Op.eq]: null },
    },
  });

  if (activeGame) {
    outcome('A game has already been started in this channel');
    return;
  }

  const tables = await TeamTable.findAll({ where: { teamId } });
  if (!tables || tables.length === 0) {
    outcome('You have no foosball tables registered for your team!');
    return;
  }

  for (const table of tables) {
    const { tableId } = table;

    activeGame = await Game.findOne({ where: { tableId, finishDate: { [Op.eq]: null } } });
    if (activeGame) {
      continue;
    }

    await Game.create({
      teamId,
      channelId,
      tableId,
      createUser: event.user,
    });

    outcome('Your game has been created.\r\nGet ready to rumble!');
    return;
  }

  outcome('Could not find an available table.\r\nClear one up!');
}
