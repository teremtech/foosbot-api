export default async function (db, gameId, team = undefined) {
  return (await db.gameScore.count({ where: { gameId, team } })) || 0;
}
