import dotenv from 'dotenv';

import FoosBot from './FoosBot';

// Run dotenv so .ENV variables are loaded
dotenv.config();

const foosBot = new FoosBot();

foosBot.listen(3000);
