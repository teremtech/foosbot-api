module.exports = (sequelize, DataTypes) => {
  const player = sequelize.define('player', {
    playerId: {
      type: DataTypes.STRING,
      unique: true,
      primaryKey: true,
      allowNull: false,
    },
  }, {
    tableName: 'player',
  });

  return player;
};
