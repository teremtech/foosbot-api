module.exports = (sequelize, DataTypes) => {
  const gameGoal = sequelize.define('gameGoal', {
    gameId: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    team: {
      type: DataTypes.ENUM('A', 'B'),
      allowNull: false,
    },
    playerId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
  }, {
    tableName: 'game_goal',
  });

  return gameGoal;
};
