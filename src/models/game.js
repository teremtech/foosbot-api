module.exports = (sequelize, DataTypes) => {
  const game = sequelize.define('game', {
    teamId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    channelId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    tableId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    gameId: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    maximumGoals: {
      type: DataTypes.INTEGER,
      defaultValue: 10,
      allowNull: false,
      validate: { min: 1 },
    },
    createUser: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    startDate: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    finishDate: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    tableName: 'game',
  });

  return game;
};
