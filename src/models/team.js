module.exports = (sequelize, DataTypes) => {
  const team = sequelize.define('team', {
    teamId: {
      primaryKey: true,
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    name: DataTypes.STRING,
    accessToken: DataTypes.STRING,
  }, {
    tableName: 'team',
  });

  return team;
};
