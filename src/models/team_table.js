module.exports = (sequelize, DataTypes) => {
  const teamTable = sequelize.define('teamTable', {
    teamId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    tableId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    tableName: 'team_table',
  });

  return teamTable;
};
