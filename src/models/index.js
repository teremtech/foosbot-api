const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const settings = {
  host: process.env.DATABASE_HOST,
  port: process.env.DATABASE_PORT,
  database: process.env.DATABASE_DATABASE,
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  dialect: 'postgres',
  logging: false,
  define: {
    timestamps: true,
    paranoid: true,
    createdAt: 'createDate',
    deletedAt: 'deleteDate',
    updatedAt: 'updateDate',
  },
};

const sequelize = new Sequelize(settings);
const basename = path.basename(__filename);
const models = fs.readdirSync(__dirname).filter(file => (
  file.indexOf('.') !== 0 && file !== basename
  && (file.toLowerCase().endsWith('.js') || file.toLowerCase().endsWith('.mjs'))
));

const db = {};

for (const file of models) {
  const model = sequelize.import(path.join(__dirname, file));
  db[model.name] = model;
}

for (const model in db) {
  if (!Object.prototype.hasOwnProperty.call(db, model)) continue;
  if (!Object.prototype.hasOwnProperty.call(db[model], 'associate')) continue;

  db[model].associate(db);
}

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
