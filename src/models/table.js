module.exports = (sequelize, DataTypes) => {
  const table = sequelize.define('table', {
    tableId: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    maximumParticipantsPerTeam: {
      type: DataTypes.INTEGER,
      defaultValue: 2,
      allowNull: false,
      validate: { min: 1 }, // safeguard to prevent 0 participants
    },
  }, {
    tableName: 'table',
  });

  return table;
};
