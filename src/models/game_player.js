module.exports = (sequelize, DataTypes) => {
  const gamePlayer = sequelize.define('gamePlayer', {
    gameId: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    team: {
      type: DataTypes.ENUM('A', 'B'),
      allowNull: false,
    },
    playerId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    tableName: 'game_player',
  });

  return gamePlayer;
};
