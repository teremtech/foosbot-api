import celebrate from 'celebrate';
import express from 'express';
import passport from 'passport';
import SlackClient from '@slack/client';
import SlackEvents from '@slack/events-api';
import SlackStrategy from '@aoberoi/passport-slack';

import boom from './middleware/boom';
import db from './models';
import eventHandlers from './events';
import routes from './routes';

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

export default class FoosBot {
  constructor() {
    // Initialise Express
    this.app = express();

    this.initialiseDatabase();
    this.initialisePassport();
    this.initialiseSlackEvents();

    this.app.use(express.json());
    this.app.use(boom);
    this.app.use(celebrate.errors());
    this.app.use(routes);
  }

  initialiseDatabase() {
    this.db = db;
    this.db.sequelize.sync();
    this.app.set('db', this.db);
  }

  initialiseSlackEvents() {
    // Create slackEvents instance
    this.slackEvents = SlackEvents.createEventAdapter(process.env.SLACK_SIGNING_SECRET, {
      includeBody: true,
    });

    // Load individual handlers
    for (const handler of eventHandlers) {
      console.log(`Loading ${handler.id} handler...`);
      this.slackEvents.on(handler.id, handler.handler.bind(this));
    }

    this.app.use('/slack/events', this.slackEvents.expressMiddleware());
  }

  initialisePassport() {
    this.clients = {};

    passport.use(new SlackStrategy.default.Strategy({
      clientID: process.env.SLACK_CLIENT_ID,
      clientSecret: process.env.SLACK_CLIENT_SECRET,
      skipUserProfile: true,
    }, this.saveTeam.bind(this)));

    this.app.use(passport.initialize());

    // Middleware to allow endpoints to get Slack clients
    this.app.use((req, res, next) => {
      req.getSlackClientByTeamId = this.getSlackClientByTeamId.bind(this);
      next();
    });
  }

  async listen(port = process.env.LISTEN_PORT || 3000) {
    this.app.listen(port);
    console.log(`Listening on port ${port}`);
  }

  saveTeam(accessToken, scopes, team, extra, profiles, done) {
    const teamId = team.id;
    const { name } = team;

    this.db.team.upsert({ teamId, name, accessToken });

    done(null, {});
  }

  async getSlackClientByTeamId(teamId) {
    // eslint-disable-next-line no-param-reassign
    teamId = teamId.trim().toUpperCase();

    if (this.clients[teamId]) {
      return this.clients[teamId];
    }

    const team = await this.db.team.findByPk(teamId);
    if (!team) {
      return undefined;
    }

    this.clients[teamId] = new SlackClient.WebClient(team.accessToken);

    return this.clients[teamId];
  }
}
